package no.noroff.SqlRestDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlRestDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlRestDemoApplication.class, args);
	}

}
