package no.noroff.SqlRestDemo.data_access;

import no.noroff.SqlRestDemo.models.Customer;
import org.sqlite.jdbc4.JDBC4PreparedStatement;

import java.sql.*;
import java.util.ArrayList;

public class CustomerRepository {
    // Setting up the connection object we need.
    private String URL = "jdbc:sqlite::resource:Northwind_small.sqlite";
    private Connection conn = null;

    /*
     Setup methods to manipulate database, using conn = DriverManager.getConnection(URL);
     and prepared statements.
    */

    //CRUD

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customers = new ArrayList<>();
        // ---
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Id, CompanyName, ContactName, Phone FROM customer");
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customers.add( new Customer(
                        set.getString("Id"),
                        set.getString("CompanyName"),
                        set.getString("ContactName"),
                        set.getString("Phone")
                ));
            }
            System.out.println("Get all went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        // ---
        return customers;
    }

    public Customer getSpecificCustomer(String id){
        Customer customer = null;
        // ---
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("SELECT Id, CompanyName, ContactName, Phone " +
                            "FROM customer WHERE Id=?");
            prep.setString(1,id);
            ResultSet set = prep.executeQuery();
            while(set.next()){
                customer = new Customer(
                        set.getString("Id"),
                        set.getString("CompanyName"),
                        set.getString("ContactName"),
                        set.getString("Phone")
                );
            }
            System.out.println("Get specific went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        // ---

        return customer;
    }

    public Boolean addCustomer(Customer customer){
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("INSERT INTO customer(Id,CompanyName,ContactName,Phone)" +
                            " VALUES(?,?,?,?)");
            prep.setString(1,customer.getCustomerId());
            prep.setString(2,customer.getCompanyName());
            prep.setString(3,customer.getContactName());
            prep.setString(4,customer.getPhone());

            int result = prep.executeUpdate();
            success = (result != 0); // if res = 1; true

            System.out.println("Add went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        // ---
        return success;
    }

    public Boolean updateCustomer(Customer customer){
        Boolean success = false;
        try{
            // connect
            conn = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    conn.prepareStatement("UPDATE customer SET Id=?, CompanyName=?, ContactName=?,Phone=?" +
                            " WHERE Id=?");
            prep.setString(1,customer.getCustomerId());
            prep.setString(2,customer.getCompanyName());
            prep.setString(3,customer.getContactName());
            prep.setString(4,customer.getPhone());
            prep.setString(5,customer.getCustomerId());

            int result = prep.executeUpdate();
            success = (result != 0); // if res = 1; true

            System.out.println("Update went well!");

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                conn.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        // ---
        return success;
    }
}
